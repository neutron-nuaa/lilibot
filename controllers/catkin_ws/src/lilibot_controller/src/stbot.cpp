/*
 * Written by Sun Tao 2018-4-17
 * 
 */

#include "../../../../../../../controllers/stbot/genesis/controller.h"

// Main code:
int main(int argc,char* argv[])
{
    Controller controller(argc,argv);
    while(controller.run()){
        }
    return(0);
}
