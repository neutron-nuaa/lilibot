### Folder framework

- controller.cpp is the main function of the control algorithm, it is called by main.cpp/stbot.cpp of the project
- controller -> modularNeuralController -> modularNeural 
- controller -> rosClass

**The relationship among these source files is described in below figure.**

<img src="./../../doc/images/controller_framework.png" alt="controller_framework" style="zoom:60%;" />

- controller.cpp works as the main function of the control algorithm. 
- rosClass.cpp realizes the ROS interfaces to communicate with ROS topics, ROS parameter servers, and ROS node of robot
- moduralNeuroController.cpp call modularNeural.cpp to generate control commands, get sensory feedback
- modularNeural.cpp includes all used modules in the control algorithm

**Shell scripts for automatically launching the simulation**
lauSimExp folder contain some shell scripts for automatically launching simulation. It can repeatedly execute simulation with specified lists of parameter values, such as MI values, ground roughness, floor inclination, as well as different control mechanisms, such as phase modulation (PM), phase resetting (PR), and adaptive physical and neural communications (APNC).
