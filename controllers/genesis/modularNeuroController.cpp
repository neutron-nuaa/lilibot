/***
Data:2017/12/19
Descriptions: the main interface of controller
Authors: sun tao
 *****/
#include "modularNeuroController.h"
#define PLOT_LEG 0
using namespace matrix;
using namespace std;
namespace stcontroller {
    ModularNeuroController::ModularNeuroController(const ModularNeuroControllerConf& c): conf(c) {

        //1.1) initial filters for sensors
        filterGRF.resize(conf.leg_num);
        filterPose.resize(conf.pose_num); // 3 orientation and 3 position
        filterPosition.resize(conf.motor_num);
        filterVelocity.resize(conf.motor_num);
        filterCurrent.resize(conf.motor_num);
        filterVoltage.resize(conf.motor_num);

        //1.2) initial sensor value storage
        GRForce.resize(conf.leg_num);//Ground reaction force
        ForceNoise.resize(conf.leg_num);//manual noise of ground reaction force
        Pose.resize(conf.pose_num); //3 orentation and 3 position dimensions
        JointPosition.resize(conf.motor_num); //12 joints
        JointVelocity.resize(conf.motor_num); //12 joints
        JointCurrent.resize(conf.motor_num); //12 joints
        JointVoltage.resize(conf.motor_num); //12 joints
        asf.resize(conf.leg_num*2);// Adaptive sensory feedback of the SFM model
        aci.resize(conf.leg_num*2);// Adaptive control input of the ACI model

        //1.3) initialize modules
        init();

        //1.4) predefine variables for adaptive neural couyplings
        ANCstability =0.0;
        ANC_RP_NC.set(conf.leg_num,conf.leg_num);
        ANC_RP.set(conf.leg_num,conf.leg_num);

    }

    ModularNeuroController::~ModularNeuroController() {
        delete mnc;// modular neural control
        delete anc;// autonomous neural connection
        delete FAVes;// frequency adaptation based on vestibular feedback
        delete manipulator;// auto manipulate walking direction

        for(std::vector<lowPass_filter *>::iterator it= filterGRF.begin(); it != filterGRF.end();it++ )
            delete *it;
        filterGRF.clear();

        for(std::vector<lowPass_filter *>::iterator it= filterPose.begin(); it != filterPose.end();it++ )
            delete *it;
        filterPose.clear();

        for(std::vector<lowPass_filter *>::iterator it= filterPosition.begin(); it != filterPosition.end();it++ )
            delete *it;
        filterPosition.clear();

        for(std::vector<lowPass_filter *>::iterator it= filterVelocity.begin(); it != filterVelocity.end();it++ )
            delete *it;
        filterVelocity.clear();

        for(std::vector<lowPass_filter *>::iterator it= filterVoltage.begin(); it != filterVoltage.end();it++ )
            delete *it;
        filterVoltage.clear();
    }

    void ModularNeuroController::init() {

        x.resize(conf.sensor_num);
        y.resize(conf.motor_num);

        //controller running time
        t = 0;

        //the second variable corresponds to the number of cpgs to create,i.e. fourlegs=4
        mnc = new ModularNeural(conf.leg_num);// modular neural control
        anc = new AutoNeuralConnection(conf.leg_num);//estimate movement phase diff and its stability
        FAVes = new FrequencyAdaptationBasedVes();// frequancy adaptation based on vestibular feedback
        manipulator = new Manipulation(); // handle robot walking direction


        // create filter objects, low pass filter to clean the feedback signals of the different legs,cut off frequency has been set to 0.3,
        //setting it to a frequency bigger than 0.4 results in too much sensitive adaptive oscillator's response
        for (unsigned int i = 0; i < filterGRF.size(); i++)
            filterGRF.at(i) = new lowPass_filter(0.6);
        for (unsigned int i = 0; i < filterPose.size(); i++)
            filterPose.at(i) = new lowPass_filter(0.4);
        for (unsigned int i = 0; i < filterPosition.size(); i++)
            filterPosition.at(i) = new lowPass_filter(0.4);
        for (unsigned int i = 0; i < filterVelocity.size(); i++)
            filterVelocity.at(i) = new lowPass_filter(0.4);
        for (unsigned int i = 0; i < filterCurrent.size(); i++)
            filterCurrent.at(i) = new lowPass_filter(0.7);
        for (unsigned int i = 0; i < filterVoltage.size(); i++)
            filterVoltage.at(i) = new lowPass_filter(0.7);



        std::cout<<"##-----------------------------------------##"<<std::endl;
        std::cout<<"Initial Modulaer Neural network successfuly !"<<std::endl;
        std::cout<<"---------------------------------------------"<<std::endl;
    }

    /* set the controller paramter values based on ros parameter server values, configure controller */
    void ModularNeuroController::setParameters(const std::vector<float>& param){
        assert(param.size()==conf.param_num);
        conf.stUSER_MACRO=param[0];
        conf.stCPGSType=param[1];
        conf.stCPGMi=param[2];
        conf.stCPGPGain=param[3];
        conf.stCPGPThreshold=param[4];
        conf.stPCPGBeta=param[5];
        for(unsigned int i=0;i<conf.leg_num;i++){
            conf.stPsnInput.at(i)=param[6*i+6];
            conf.stVrnHipInput.at(i)=param[6*i+7];
            conf.stVrnKneeInput.at(i)=param[6*i+8];

            conf.stMNBias1.at(i)=param[6*i + 9];
            conf.stMNBias2.at(i)=param[6*i + 10];
            conf.stMNBias3.at(i)=param[6*i + 11];
        }
    }

    /* get ros server parameter values from this controller, upodate ros param server based on the contoller status*/
    void ModularNeuroController::getParameters(std::vector<float>& param)const{
        assert(param.size()==conf.param_num);
        param[0] = conf.stUSER_MACRO;
        param[1] = conf.stCPGSType;
        param[2] = conf.stCPGMi;
        param[3] = conf.stCPGPGain;
        param[4] = conf.stCPGPThreshold;
        param[5] = conf.stPCPGBeta;

        for(unsigned int i=0;i<conf.leg_num;i++){
            param[6*i+6]=conf.stPsnInput.at(i);
            param[6*i+7]=conf.stVrnHipInput.at(i);
            param[6*i+8]=conf.stVrnKneeInput.at(i);

            param[6*i+9]=conf.stMNBias1.at(i);
            param[6*i+10]=conf.stMNBias2.at(i);
            param[6*i+11]=conf.stMNBias3.at(i);
        }
    }   

    //implement controller here
    void ModularNeuroController::step(const parameter* x_, int number_sensors, parameter* y_, int number_motors) {
        assert(number_sensors == conf.sensor_num);
        assert(number_motors == conf.motor_num);

        //0) Sensor inputs/scaling 
        for (unsigned int i = 0; i < x.size(); i++) {
            x.at(i) = x_[i];
        }
        //0.1) filter sensor value
        //0.11) filter joint angle feedback
        for (unsigned int i = 0; i < filterPosition.size(); i++){
            JointPosition.at(i) = filterPosition.at(i)->update(x.at(i));//S1R0_as
            JointVelocity.at(i) = filterVelocity.at(i)->update(x.at(i+conf.motor_num));
            JointCurrent.at(i) = filterCurrent.at(i)->update(x.at(i+2*conf.motor_num));
            JointVoltage.at(i) = filterVoltage.at(i)->update(x.at(i+3*conf.motor_num));
        }
        //0.12) filter Pose, roll, pitch, yaw
        for (unsigned int i = 0; i < filterPose.size(); i++)
            Pose.at(i) = filterPose.at(i)->update(x.at(i+4*conf.motor_num));// 12 position, 12 velocity, 12 current, 12 voltage, 9 pose, and 4 grf
        //0.13) filter ground reaction force
        for(unsigned int i = 0; i < filterGRF.size(); i++){
            GRForce.at(i) = filterGRF.at(i)->update(x.at(i+4*conf.motor_num + conf.pose_num)); //filtering of the feedback signal,force signal
        }


        //-------------------------------------------------------------------------//
        //0.15) get the CPGSType from ros param
        CPGSType=(CPGSTYPE)conf.stCPGSType; 
        //1) set the simulation parameters
        //1.1) set the CPGs' parameters
        for (unsigned int i = 0; i < conf.leg_num; i++) {
            mnc->setCpgMi(i, conf.stCPGMi);//fix MI when cpgstype is SP
            mnc->setPCPGbeta(i, conf.stPCPGBeta);
            mnc->setMNBias(i,conf.stMNBias1.at(i),conf.stMNBias2.at(i),conf.stMNBias3.at(i));
        }
        mnc->setCPGModulationParameters(conf.stCPGPGain, conf.stCPGPThreshold);

        //1.2) upper and out layer input ,update inputNeuron Input,1-侧摆关节抑制转移，2-Psn，3-VRN,4-待用
        manipulator->setInput(0.0, 0.0, 0.0, Pose.at(2), Pose.at(1), mnc->getPMNOutput(1), mnc->getPMNOutput(2));// expected_speed, direction,actual_speed, actual_direction=yaw, pitch, pmn1, pmn2
        manipulator->step();
        for(unsigned int i =0;i<conf.leg_num;i++)
            mnc->setInputNeuronInput(i,conf.stJ1Input.at(i),conf.stPsnInput.at(i), conf.stVrnHipInput.at(i) + 0.0*manipulator->getHipVRNOutput(i), conf.stVrnKneeInput.at(i) + 0.0*manipulator->getKneeVRNOutput(i));

        //2) update orientation for body of attitude control, frequancy adaptation
        mnc->setAttituteInput(Pose);//update the attitude input
        FAVes->setInput(Pose.at(0), Pose.at(1));// roll, pitch
        FAVes->step();

        //3.0) update CPGS criterion paramters movement phase diff and its stability
        for(unsigned int i=0;i<conf.leg_num;i++){
            anc->setInput(i,SO2CPGOuts(mnc->getCpgOut0(i),mnc->getCpgOut1(i)));// step signals
        }
        anc->step();// automatic neural connections
        ANC_RP = anc->getPhaseDiff();// get realtive phase relationship
        ANCstability = anc->getPhaseStability();// get stability of the phase difference
        ANC_RP_NC = ANC_RP;// sp_cmatrix is formed when CPGType is decoupled

        // adaptive neural communication/conenctions
        if(false && anc->getANCtrigger()){// NOTE: this rountine was closed
            if(CPGSType != CPG_ANC){//autonomous neural connection 
                conf.stsetCPGS.State = true;//set CPGSType to Ros param
                conf.stCPGSType = CPG_APC;//set CPGSType to ROS param
                ANC_RP_NC = ANC_RP;// sp_cmatrix is formed when CPGType is decoupled
                cout<<"##-----INFO---##"<<endl;
                cout<<"Now CPGType is APC (adaptvie physical communication)"<<endl;
                cout<<"----------------"<<endl;
                cout<<ANC_RP<<endl;
            }
        }

        //3.3) set the sensory feedback and update step of each leg
        for (unsigned int i = 0; i < conf.leg_num; i++) {
            mnc->setJointSensorAngle(i, JointPosition.at(3 * i),
                    JointPosition.at(3 * i + 1), JointPosition.at(3 * i + 2));
            if(conf.stUSER_MACRO==1){// add noisy to sensory feedback
                ForceNoise.at(i) = 6.0*std::rand()/float(RAND_MAX);
                mnc->setFootSensorForce(i, GRForce.at(i) + ForceNoise.at(i));//GRF feedback with noise
            }else{
                mnc->setFootSensorForce(i,GRForce.at(i));//GRF feedback
            }
            mnc->setACIMatrix(ANC_RP_NC);//gait term, autonomous neural connection`
        }

        //3.4) Update steps after all sensory information are update 
        for (unsigned int i = 0; i < conf.leg_num; i++) {
            mnc->step(i,CPGSType);
        }

        // 4) control the joints
        for(unsigned int i=0;i<y.size();i++)
            y_[i] = mnc->getPMNOutput(i);

        // fix right front leg to neutral position
        if(conf.stUSER_MACRO==2){
            y_[1]=0;
            y_[2]=0;
        }

        //5) update the inspected value
        updateData();

        //6) update the time 
        t++;
    }

    //utility to draw outputs of the neurons
    void ModularNeuroController::updateData() {

        for(unsigned int i=0;i<conf.leg_num;i++){
            //1) CPG
            conf.stCPGN0.at(i) =mnc->getCpgOut0(i);
            conf.stCPGN1.at(i) =mnc->getCpgOut1(i);
            //2) PCPG
            conf.stPCPGN0.at(i) = mnc->getPcpgOutput(i, 0);
            conf.stPCPGN1.at(i) = mnc->getPcpgOutput(i, 1);
            //3) PSN
            conf.stPSN10.at(i) = mnc->getPsnOutput(i, 10);
            conf.stPSN11.at(i) = mnc->getPsnOutput(i, 11);
            //4) VRN
            conf.stVRNHip.at(i) = mnc->getHipVrnOutput(i);
            conf.stVRNKnee.at(i) = mnc->getKneeVrnOutput(i);
            //5) attiReflex of one leg
            conf.stReflexOutN0.at(i)= mnc->getVestibularReflexOutput(i,0);
            conf.stReflexOutN1.at(i)= mnc->getVestibularReflexOutput(i,1);
            conf.stReflexOutN2.at(i)= mnc->getVestibularReflexOutput(i,2);
            //6) PMN
            conf.stPMN0.at(i) = mnc->getPmnOutput(i, 0);
            conf.stPMN1.at(i) = mnc->getPmnOutput(i, 1);
            conf.stPMN2.at(i) = mnc->getPmnOutput(i, 2);
            //8) AFG adaptive feedback gain
            conf.stAFGOut.at(i) = mnc->getAFGOutput(i);
            //9) FA frequency adaptation
            conf.stFAOut.at(i) = mnc->getFAOutput(i);
            //10) sensory feedback term in CPG
            conf.stPAOut0.at(i) = mnc->getCpgAsf(i,0);
            conf.stPAOut1.at(i) = mnc->getCpgAsf(i,1);
            //11) NP for ground reactive force
            conf.stNPOut.at(i) = mnc->getNPOutput(i);
            //12) adaptive control input term in CPG
            conf.stACIOut0.at(i)=mnc->getCpgAci(i,0);// the gait term in CPG
            conf.stACIOut1.at(i)=mnc->getCpgAci(i,1);// the gait term in CPG
            //13) forward model of AFG
            conf.stFMOut.at(i)=mnc->getAFGfmOutput(i);

        }

        // get the ASF and ACI outputs from SPCPG, their modules values
        for(unsigned int idx=0;idx<conf.leg_num;idx++){
            asf.at(2*idx)=mnc->getCpgAsf(idx,0);
            asf.at(2*idx+1)=mnc->getCpgAsf(idx,1);
            aci.at(2*idx)=mnc->getCpgAci(idx,0);
            aci.at(2*idx+1)=mnc->getCpgAci(idx,1);
        }


    }

    void ModularNeuroController::getOutData(std::vector<float>& data){
        /**
         * get out data for online display in Vrep simulation 
         */

        data.clear();
        for(uint8_t i=0;i<conf.leg_num;i++){
            //1) CPG
            data.push_back(conf.stCPGN0.at(i));//0
            data.push_back(conf.stCPGN1.at(i));//1
            //2) PCPG
            data.push_back(conf.stPCPGN0.at(i));//2
            data.push_back(conf.stPCPGN1.at(i));//3
            //3) PSN
            data.push_back(conf.stPSN10.at(i));//4
            data.push_back(conf.stPSN11.at(i));//5
            //4) VRN
            data.push_back(conf.stVRNHip.at(i));//6
            data.push_back(conf.stVRNKnee.at(i));//7
            //5) Reflex
            data.push_back(conf.stReflexOutN0.at(i));//8
            data.push_back(conf.stReflexOutN1.at(i));//9
            data.push_back(conf.stReflexOutN2.at(i));//10
            //7) PMN
            data.push_back(conf.stPMN0.at(i));//11
            data.push_back(conf.stPMN1.at(i));//12
            data.push_back(conf.stPMN2.at(i));//13
            //9) gait term in CPG
            data.push_back(conf.stACIOut0.at(i));//14
            data.push_back(conf.stACIOut1.at(i));//15
            //10)adaptive sensory feedback gain
            data.push_back(conf.stAFGOut.at(i));//16
            //11) frequency adaptation
            data.push_back(conf.stFAOut.at(i));//17
            //12) phase adaptation--sensory feedback term
            data.push_back(conf.stPAOut0.at(i));//18
            data.push_back(conf.stPAOut1.at(i));//19
            //13) formard model
            data.push_back(conf.stFMOut.at(i));//20
        }
        //10) phase and its stability
        data.push_back(ANC_RP.val(0,1));//84
        data.push_back(ANC_RP.val(0,2));//85
        data.push_back(ANC_RP.val(0,3));//86
        data.push_back(ANCstability);//87
        data.push_back(anc->getANCtrigger());//88
        //data.push_back(FAVes->getOutput());//89
        data.push_back(Pose.at(1));//89
        data.push_back(GRForce.at(0));//89--RF
        data.push_back(GRForce.at(1));//90--LH
        data.push_back(GRForce.at(2));//91--LH
        data.push_back(GRForce.at(3));//92--LH
    }


    void ModularNeuroController::storedData(std::vector<float>& cpg_data, std::vector<float>& commands_data, std::vector<float>& sensory_data, std::vector<float>& parameters_data, std::vector<float>& modules_data) {
        /**
         * Store data in files
         */

        //1)  CPG
        cpg_data.clear();
        for(uint8_t i=0;i<conf.leg_num;i++){
            //1) CPG, 0-7
            cpg_data.push_back(conf.stCPGN0.at(i));
            cpg_data.push_back(conf.stCPGN1.at(i));
        }
        //2) commands
        commands_data.clear();
        for(uint8_t i=0;i<conf.leg_num;i++){
            commands_data.push_back(conf.stPMN0.at(i));//12
            commands_data.push_back(conf.stPMN1.at(i));//13
            commands_data.push_back(conf.stPMN2.at(i));//14
        }

        //3) sensory feedback
        sensory_data.clear();
        //joint feedback
        for(unsigned int idx=0; idx<JointPosition.size(); idx++) 
            sensory_data.push_back(JointPosition.at(idx));///117-128
        for(unsigned int idx=0; idx<JointVelocity.size(); idx++) 
            sensory_data.push_back(JointVelocity.at(idx));///117-128
        for(unsigned int idx=0; idx<JointCurrent.size(); idx++) 
            sensory_data.push_back(JointCurrent.at(idx));///117-128
        for(unsigned int idx=0; idx<JointVoltage.size(); idx++) 
            sensory_data.push_back(JointVoltage.at(idx));///129-140
        // oirentation
        for(unsigned int idx=0;idx<Pose.size();idx++)
            sensory_data.push_back(Pose.at(idx));
        // GRF
        for(uint8_t i=0;i<conf.leg_num;i++){
            sensory_data.push_back(GRForce.at(i));//11
        }
        //4) ros server parameters
        parameters_data.clear();
        parameters_data.push_back(conf.stUSER_MACRO);
        parameters_data.push_back(conf.stCPGSType);
        parameters_data.push_back(conf.stCPGMi);
        parameters_data.push_back(conf.stCPGPGain);
        parameters_data.push_back(conf.stCPGPThreshold);
        parameters_data.push_back(conf.stPCPGBeta);
        for(unsigned int i=0;i<conf.leg_num;i++){
            parameters_data.push_back(conf.stPsnInput.at(i));
            parameters_data.push_back(conf.stVrnHipInput.at(i));
            parameters_data.push_back(conf.stVrnKneeInput.at(i));

            parameters_data.push_back(conf.stMNBias1.at(i));
            parameters_data.push_back(conf.stMNBias2.at(i));
            parameters_data.push_back(conf.stMNBias3.at(i));
        }
        //5) modules
        modules_data.clear();
        modules_data.push_back(ANCstability);

        modules_data.push_back(mnc->getDFRLplasticWeight(0));
        modules_data.push_back(mnc->getDFRLplasticWeight(1));
        modules_data.push_back(mnc->getDFRLplasticWeight(2));
        modules_data.push_back(mnc->getDFRLplasticWeight(3));
        //sensory noise
        modules_data.push_back(ForceNoise.at(0));
        modules_data.push_back(ForceNoise.at(1));
        modules_data.push_back(ForceNoise.at(2));
        modules_data.push_back(ForceNoise.at(3));
        //ASF and ACI
        modules_data.push_back(asf.at(0));
        modules_data.push_back(asf.at(1));
        modules_data.push_back(asf.at(2));
        modules_data.push_back(asf.at(3));
        modules_data.push_back(asf.at(4));
        modules_data.push_back(asf.at(5));
        modules_data.push_back(asf.at(6));
        modules_data.push_back(asf.at(7));

        modules_data.push_back(aci.at(0));
        modules_data.push_back(aci.at(1));
        modules_data.push_back(aci.at(2));
        modules_data.push_back(aci.at(3));
        modules_data.push_back(aci.at(4));
        modules_data.push_back(aci.at(5));
        modules_data.push_back(aci.at(6));
        modules_data.push_back(aci.at(7));

        modules_data.push_back(conf.stFMOut.at(0));// forward model of right front leg
        modules_data.push_back(conf.stFMOut.at(1));//forward model or right hind leg
        modules_data.push_back(conf.stAFGOut.at(0));//adaptive feedback gain of right front leg
        modules_data.push_back(conf.stAFGOut.at(1));//adaptive feedback gain of right hind leg
        modules_data.push_back(conf.stNPOut.at(0));// NP_GRF of right front leg
        modules_data.push_back(conf.stNPOut.at(1));// NP_GRF of right hind leg

        modules_data.push_back(ANC_RP.val(0,1));//phase relationship
        modules_data.push_back(ANC_RP.val(0,2));//phase relationship
        modules_data.push_back(ANC_RP.val(0,3));//phase relationship
    }


    void ModularNeuroController::storedData(vector<float>& cpg_data, vector<string>& cpg_data_names, vector<float>& commands_data,  vector<string>& commands_data_names,  vector<float>& sensory_data, vector<string>& sensory_data_names, vector<float>& parameters_data, vector<string>& parameters_data_names, vector<float>& modules_data, vector<string>& modules_data_names) {
        /**
         * Store data in files
         */

        //1)  CPG
        cpg_data.clear();
        cpg_data_names.clear();
        for(uint8_t idx=0;idx<conf.leg_num;idx++){
            //1) CPG, 0-7
            cpg_data.push_back(conf.stCPGN0.at(idx));
            cpg_data.push_back(conf.stCPGN1.at(idx));
            cpg_data_names.push_back(string("CPGN0_").append(to_string(idx)));
            cpg_data_names.push_back(string("CPGN1_").append(to_string(idx)));
        }
        //2) commands
        commands_data.clear();
        commands_data_names.clear();
        for(uint8_t idx=0;idx<conf.leg_num;idx++){
            commands_data.push_back(conf.stPMN0.at(idx));//12
            commands_data.push_back(conf.stPMN1.at(idx));//13
            commands_data.push_back(conf.stPMN2.at(idx));//14
            commands_data_names.push_back(string("PMN0_Leg").append(to_string(idx)));
            commands_data_names.push_back(string("PMN1_Leg").append(to_string(idx)));
            commands_data_names.push_back(string("PMN2_Leg").append(to_string(idx)));
        }
        //3) sensory feedback
        sensory_data.clear();
        sensory_data_names.clear();
        //joint feedback
        for(uint8_t idx=0; idx<JointPosition.size(); idx++){ 
            sensory_data.push_back(JointPosition.at(idx));///117-128
            sensory_data_names.push_back(string("JointPosition_").append(to_string(idx)));
        }
        for(uint8_t idx=0; idx<JointVelocity.size(); idx++){ 
            sensory_data.push_back(JointVelocity.at(idx));///117-128
            sensory_data_names.push_back(string("JointVelocity_").append(to_string(idx)));
        }
        for(uint8_t idx=0; idx<JointCurrent.size(); idx++){
            sensory_data.push_back(JointCurrent.at(idx));///117-128
            sensory_data_names.push_back(string("JointCurrent_").append(to_string(idx)));
        }
        for(uint8_t idx=0; idx<JointVoltage.size(); idx++){ 
            sensory_data.push_back(JointVoltage.at(idx));///129-140
            sensory_data_names.push_back(string("JointVoltage_").append(to_string(idx)));
        }
        // oirentation
        for(uint8_t idx=0;idx<Pose.size();idx++){
            sensory_data.push_back(Pose.at(idx));
            sensory_data_names.push_back(string("Pose_").append(to_string(idx)));
        }
        // GRF
        for(uint8_t idx=0;idx<conf.leg_num;idx++){
            sensory_data.push_back(GRForce.at(idx));//11
            sensory_data_names.push_back(string("GRF_").append(to_string(idx)));
        }
        //4) ros server parameters
        parameters_data.clear();
        parameters_data_names.clear();
        parameters_data.push_back(conf.stCPGSType);
        parameters_data_names.push_back("CPGSType");

        parameters_data.push_back(conf.stCPGMi);
        parameters_data_names.push_back("MI");

        parameters_data.push_back(conf.stCPGPGain);
        parameters_data_names.push_back("CPGPGain");

        parameters_data.push_back(conf.stCPGPThreshold);
        parameters_data_names.push_back("CPGPThreshold");

        parameters_data.push_back(conf.stPCPGBeta);
        parameters_data_names.push_back("PCPGBeta");

        for(uint8_t idx=0;idx<conf.leg_num;idx++){
            parameters_data.push_back(conf.stPsnInput.at(idx));
            parameters_data_names.push_back(string("PSNInput_").append(to_string(idx)));
            parameters_data.push_back(conf.stVrnHipInput.at(idx));
            parameters_data_names.push_back(string("PCPGBeta_").append(to_string(idx)));
            parameters_data.push_back(conf.stVrnKneeInput.at(idx));
            parameters_data_names.push_back(string("PCPGBeta_").append(to_string(idx)));

            parameters_data.push_back(conf.stMNBias1.at(idx));
            parameters_data_names.push_back(string("MNBias1_").append(to_string(idx)));
            parameters_data.push_back(conf.stMNBias2.at(idx));
            parameters_data_names.push_back(string("MNBias2_").append(to_string(idx)));
            parameters_data.push_back(conf.stMNBias3.at(idx));
            parameters_data_names.push_back(string("MNBias3_").append(to_string(idx)));
        }
        //5) modules
        modules_data.clear();
        modules_data_names.clear();

        modules_data.push_back(ANCstability);
        modules_data_names.push_back("ANCstability");

        for(uint8_t idx=0;idx<4;idx++){ // DFRL
            modules_data.push_back(mnc->getDFRLplasticWeight(idx));
            modules_data_names.push_back(string("DFRLplasticWeight_").append(to_string(idx)));
        }

        //ASF and ACI
        for(uint8_t idx=0;idx<8;idx++){ // adaptvie sensory feedback
            modules_data.push_back(asf.at(idx));
            modules_data_names.push_back(string("ASF_").append(to_string(idx)));
        }

        for(uint8_t idx=0;idx<8;idx++){// adaptive control input
            modules_data.push_back(aci.at(idx));
            modules_data_names.push_back(string("ACI_").append(to_string(idx)));
        }

        for(uint8_t idx=0;idx<2;idx++){
            modules_data.push_back(conf.stFMOut.at(idx));// forward model
            modules_data_names.push_back(string("FM_").append(to_string(idx)));
        }

        for(uint8_t idx=0;idx<2;idx++){
            modules_data.push_back(mnc->getAFGOutput(idx));// adaptive feedback gain
            modules_data_names.push_back(string("AFG_").append(to_string(idx)));
        }

        for(uint8_t idx=0;idx<3;idx++){
            modules_data.push_back(ANC_RP.val(0,idx+1));// phase relationship
            modules_data_names.push_back(string("ANC_RP_").append(to_string(idx)));
        }
    }
}// namespace
