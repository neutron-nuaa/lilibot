#include "synapticPlasticityCpg.h"
using namespace std;
SPCPG::SPCPG(unsigned int ID,unsigned int nCPGs){ 
    this->ID = ID;
    this->nCPGs =nCPGs;
    CPGType=CPG_STAND;//default CPGTYpe is CPG_STAND =-1
    setNeuronNumber(2);// CPG is tanhFunction

    MI=0.0;//modification
    updateWeights();
    b(0,0.01);
    b(1,0.01);

    setOutput(0,0.01);
    setOutput(1,0.01);

    a_t.resize(2);//all of those is vector type
    a_t1.resize(2);
    o_t.resize(2);//all of those is vector type
    // input feedback
    //----external terms
    //- adaptvie neural connection term-
    aci = new ACI(ID,nCPGs);
    //- adaptive sensory feedback term--
    asf = new ASF(ID);
    //----internal terms
    //--phase reset term ----//
    prs =new PhaseReset(ID);
    //--phase inhibition term --//
    pib =new PhaseInhibition(ID);
    //--vestibular response term --//
    ves =new Vestibular(ID);

    // adaptive sensory feedback gain
    afg=new AdaptiveFeedbackGain(ID);
    //
    //close all module in default
    so2_gain=1.0;//open SO2 neuron module in default
    aci_gain=0.0;//shutdown this module in default
    asf_gain=0.0;// shutdown this module in default
    ves_gain = 0.0;// shutdow this module in default
    prs_gain = 0.0; //shutdown this module in default
    pib_gain=0.0;// shutdown this module in default

    //efference copy
    afg_jmc=0.0;// joint motor command
    afg_grf=0.0;
}

SPCPG::~SPCPG(){
    delete aci;
    delete asf;
    delete prs;
    delete pib;
    delete ves;
    delete afg;
}

void SPCPG::setInput(parameter grf, parameter np_grf, vector<parameter> ori, parameter gain, parameter threshold, matrixD signals, CPGSTYPE CPGType, Matrix cmatrix){

    //1) sensory adaptation, agrf adaptive ground reaction force
    asf_gamma=gain;// this is a key paramter for phase modulation
    asf_np_grf=grf;
    
    //2) Adaptive neural communication
   if(CPGType==CPG_REFLEX)// shutdown cpg activies
        setMi(0.0);
    aci_signals=signals;
    aci_cmatrix=cmatrix;

    //3) vestibular 
    ves_ori=ori;
    ves_grf=grf;

    //4) phase reset
    prs_grf=grf;
    prs_threshold=threshold;//this is a key parameter for phase resetting

    //5) phase inhibition
    pib_grf=grf;


    // Adaptive sensory adaptation
    afg_grf=np_grf;

    /* There are two key parameters: PM_gain, threshold*/
    setCPGSType(CPGType);
}

void SPCPG::setEfference(parameter jmc){
    afg_jmc=jmc;//joint motor command
}


void SPCPG::setMi(parameter mi){
    if(mi<=1.0){// aviod to destroy the robot since its frequecny is too high
        MI=mi;
    }
    if(mi>1.0){
        MI=1.0;
        cout<<"WARNNING: MI value was restricted in 1.0 in roder to avoid high frequency destroying the robot!"<<endl;
    }
    if(mi==0.0){// initialize CPG when MI euqal to original point ,mi==0.0
        MI=0.0;
        ANN::setActivity(0,0.0);
        ANN::setActivity(1,0.0);
        b(0,0.01);
        b(1,0.01);
        setOutput(0,0.01);
        setOutput(1,0.01);
    }
}


/*This fucntion specifies that which modules work depending on CPGType
 *
 * */
void SPCPG::setCPGSType(CPGSTYPE CPGType){
    if(CPGType!=this->CPGType){// does the CPGType changed
        this->CPGType=CPGType;
        switch(CPGType){
            case CPG_STAND:
                // stand output
                so2_gain=0.0;// open cpg neuron actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=0.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"Stand Up was activated"<<endl;
                break;
            case CPG_GALLOP:
                // gallop output
                so2_gain=1.0;// open cpg neuron actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=1.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"Gallop was activated"<<endl;
                break;
            case CPG_PACE:
                // pace output
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg neuron actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=1.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"Pace was activated"<<endl;
                break;
            case CPG_WALK_L:
                // a walk_d output
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg neuron actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=1.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"Walk_L was activated"<<endl;
                break;
            case CPG_WALK_D:
                // a walk_l output
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=1.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"Walk_D was activated"<<endl;
                break;
            case CPG_TROT: 
                // trot output  4
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=1.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"TROT with specified CPG phase relationship was activated"<<endl;
                break;
            case CPG_APC: // 5,adaptive physical communication
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=1.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=0.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"APC: Adaptie physical communication was automatically activated"<<endl;
                break;
            case CPG_ANC:// ANC 6, the delta was generated through tranfering the adaptive limb movement coordination relationship
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=1.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"ANC:Synaptic neural connections were automatically activated"<<endl;
                break;
            case CPG_APNC://7
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=1.0;// without GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=1.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"APNC: APC and ANC were activated"<<endl;
                break;
            case CPG_PM:// 8,fixed physical communication with predefined and fixed gain
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=1.0;// with GRF feedback-phase modulation
                prs_gain=0.0;// without phase reset
                aci_gain=0.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"PM: decoupled CPGs with a fixed sensory feedback gain was activated"<<endl;
                break;
            case CPG_PR://9
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                prs_gain=1.0;// with phase reset
                aci_gain=0.0;//without the Adaptive neual connection
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"PR: decoupled CPG with was activated"<<endl;
                break;
            case CPG_REFLEX: //10, only reflex
                // phase modulation and phase reset parameter setting
                so2_gain=1.0;// open cpg actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                aci_gain=0.0;//without the Adaptive neual connection
                prs_gain=0.0;// without phase reset
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"Reflex was activated"<<endl;
                break;
            case CPG_ONLY: //11, only CPG
                so2_gain=1.0;// open cpg actities
                asf_gain=0.0;// without GRF feedback-phase modulation
                aci_gain=0.0;//without the Adaptive neual connection
                prs_gain=0.0;// without phase reset
                ves_gain=0.0;//without vestibular-based phase modulation
                pib_gain=0.0;//without phase inhibitation
                cout<<"Only CPGs were activated"<<endl;
            default:
                perror("CPGSType don't define in cpg.cpp:334");
        }
    }
}



void SPCPG::updateWeights(){
    w(0,0,1.4);
    w(0,1,+0.18+MI);
    w(1,0,-0.18-MI);
    w(1,1,1.4);
}
void SPCPG::updateActivities(){

    //--- CPG activaities upgrade ---//
    ANN::updateActivities();
    for(unsigned int i=0;i<2;i++){
        a_t.at(i) = getActivity(getNeuron(i));
        o_t.at(i) = getOutput(getNeuron(i));
    }
    //----defining connection and sensory force feeddback--//
    aci->setInput(aci_gain,aci_signals);//aci_gain
    aci->setDelta(CPGType,aci_cmatrix);//set CPG type and neural connection gain (K)
    aci->step();
    
    //Adaptive sensory feedback gain when CPGType is APC or APNC, which output adaptive feedback gain for asf
    if((CPGType==CPG_APNC)||(CPGType==CPG_APC)){ 
        afg->setInput(afg_grf, afg_jmc);
        afg->step();//input x,d or,x,y
        asf_gamma=afg->getOutput();
    }

    //-----GRF-based phase modulation----//
    asf->setInput(asf_gain, asf_gamma, asf_np_grf,o_t);//asf_gain
    asf->step();

    //-----vestibular-based phase modulation----//
    ves->setInput(ves_gain,ves_ori,ves_grf,a_t); //ves_gain 
    ves->step();

    //-----phase resetting----//
    prs->setInput(prs_gain, a_t,prs_grf, prs_threshold);// prs_gain, threshold
    prs->step();

    //-----phasse inhibit----//
    pib->setInput(pib_gain, pib_grf);
    pib->step();
    
    // the sum of these terms
    for(unsigned int i=0;i<2;i++){
        a_t1.at(i) = so2_gain*a_t.at(i) + aci->getOutput(i) - asf->getOutput(i) + prs->getOutput(i)  + ves->getOutput(i)  + pib->getOutput(i);
        setActivity(i, a_t1.at(i));
    }

}

void SPCPG::updateOutputs(){
    ANN::updateOutputs();
}
void SPCPG::step(){
    updateWeights();
    updateActivities();
    updateOutputs();
}

parameter SPCPG::getASFOutput(unsigned int index)const{
    assert(index<2);
    return asf->getOutput(index);
}


// get the Output of the adaptive feedback gain
parameter SPCPG::getAFGOutput()const{
    return afg->getOutput();
}


// get the Output of the forward model
parameter SPCPG::getFMOutput()const{
    return afg->getFMOutput();
}

parameter SPCPG::getACIOutput(unsigned int index)const{
    assert(index<2);
    return aci->getOutput(index);
}







//-------------------------adaptive sensory feedback function class ------//
ASF::ASF(unsigned int ID){
    this->ID=ID;
    gain=1.0; // gain deterime whether the module will be opened or not
    output.resize(2);
    o_t.resize(2);
    gamma = 0.0;
    grf = 0.0;
    mg=2.5*10;// lilibot weight

}

void ASF::setInput(parameter gain, parameter gamma, parameter grf, vectorD o_t){// gamma is a gain
    this->gain =gain;// for switch this module
    this->gamma=gamma;// for adjust the strength of this module
    this->grf =grf;
    this->o_t =o_t;
}

void ASF::step(){

    output.at(0)= gain*gamma*grf/mg*cos(o_t.at(0));// gamma is for adjust the strength of the module, gain is for turn on or off the module
    output.at(1)= gain*gamma*grf/mg*sin(o_t.at(1));// gamma is for adjust the strength of the module, gain is for turn on or off the module
}

parameter ASF::getOutput(unsigned int index)const{
    assert(index<2);
    return output.at(index);
}


//--------------------PhaseReset class----------------//
PhaseReset::PhaseReset(unsigned int ID){
    Diracs =0.0;
    a_t.resize(2);
    a_t_old.resize(2);
    grf = 0.0;
    grf_old = 0.0;
    threshold = 0.2; // default value
    mg=2.5*10;// robot weight
    leg_num=4.0;// robot leg number
    reset.resize(2);
}
void PhaseReset::setInput(parameter gain, vectorD a_t,parameter grf, parameter threshold){
    assert(a_t.size()==2);
    this->gain = gain;
    this->a_t =  a_t;
    this->grf = grf;
    this->threshold = threshold;
}
void PhaseReset::step(){
    /* version I:
     * This version can work like GRF modulation to produce a stable gaits, 
     * but it is not the right phase reset. For the improved one, please see version II in nexts.

    
    if((grf > threshold)&&(grf_old <= threshold)){ // threshold = 0.2 is vital important, it casued the reset moments
        Diracs =1.0;
    }else{
        Diracs =0.0;
    }
    reset.at(0) = ( 1.0 - a_t.at(0))*Diracs;
    reset.at(1) = - a_t.at(1)*Diracs;
    grf_old = grf;
    */

    /*Version II:
     * The phase reset, it has seseral key points:
     * 1) If the actual touch mement is ahead of the expected one, then reset the phase, let the CPG oscillation return move
     * 2) Find the activity value when the direction swithing, just opposite the sign of the activity of O2.
     * 3)O1 used to control hip joint, whiel knee joint was activated by O1 joint. 
     * 4) The degree of the knee joint movement is decieded by GRF value.
     
    if((grf > 0.2)&&(grf_old <= 0.2)){// Indicate the actual touch moment
        if(a_t.at(0)>a_t_old.at(0)){// if signal is still increase for hip2 joint forward movement.
            Diracs =1.0;
        }else{
           Diracs =0.0;
        }
    }else{
        Diracs =0.0;
    }
    reset.at(0) = (1-a_t.at(0))*Diracs;
    reset.at(1) = - 1.0*a_t.at(1)*Diracs;// maybe use hieracy CPG phase process
    grf_old = grf;

    a_t_old.at(0)=a_t.at(0);
    a_t_old.at(0)=a_t.at(0);
    */

    /* Version III
     * Induce robot weight mg to normalize the PR parameter F_t threshold
     *
     * */
    if((grf > threshold*mg/leg_num)&&(grf_old <= threshold*mg/leg_num)){ // threshold = 0.2 is vital important, it casued the reset moments
        Diracs =1.0;
    }else{
        Diracs =0.0;
    }
    reset.at(0) = ( 1.0 - a_t.at(0))*Diracs*gain;//gain equal to 1 for turn on this module, or 0 for turn off this module
    reset.at(1) = - a_t.at(1)*Diracs*gain;// gain equal to 1 for turn on this module, or 0 for turn off this module
    grf_old = grf;


}
parameter PhaseReset::getOutput(unsigned int index)const{
    assert(index < 2);
    return reset.at(index);
}

//-----------PhaseInhibition class-----//
/*
 * This class realize the function to inhibit the CPG phase according with 
 * the GRF information.
 *
 */
PhaseInhibition::PhaseInhibition(unsigned int ID){
    gain=1.0;// gain deterime whether the module will be opened or not
    Ch=0.06;
    grf=0.0;
    Tduration =0.0;
    inhibition.resize(2);
}
void PhaseInhibition::setInput(parameter gain, parameter grf){
    this->gain=gain;
    this->grf =grf;
}
void PhaseInhibition::step(){
    if(grf >0.3)
        Tduration = 1.0;
    else{
        Tduration = 0.0;
    }
    inhibition.at(0)=0.0*gain;
    inhibition.at(1)=-Ch*grf*Tduration*gain;
}

parameter PhaseInhibition::getOutput(unsigned int index)const{
    assert(index<2);
    return inhibition.at(index);
}

//-------------------------vestibular response class ------//
Vestibular::Vestibular(uint8_t leg){
    assert(leg>=0);
    ves.resize(2);
    roll = 0.0;
    pitch = 0.0;
    grf = 0.0;
    gain=1.0;// gain deterime whether the module will be opened or not
    switch(leg){
        case 0:
        case 1:
            roll_gain= 5;
            break;
        case 2:
        case 3:
            roll_gain= -5;
            break;
        default:
            perror("leg number is wrong in vestibular response");
    }

    switch(leg){
        case 0:
        case 2:
            pitch_gain= 5.8;
            break;
        case 1:
        case 3:
            pitch_gain= -5.8;
            break;
        default:
            perror("leg number is wrong in vestibular response");
    }

}

void Vestibular::setInput(parameter gain, vector<parameter> ori, parameter grf,vectorD a_t){
    this->gain = gain;
    this->roll = ori.at(0);
    this->pitch = ori.at(1);
    this->grf = grf;
    this->a_t = a_t;
}

void Vestibular::step(){
    /*
     * Through changing the pitch_gain from 5.0 to 6.0, the gaits changes from pace to gallop
     * The gain determines the magnitude of the effects of this class
     * */
    ves.at(0)= gain*tanh(roll_gain*roll+ pitch_gain*pitch)*cos(a_t.at(0));
    ves.at(1)= gain*tanh(roll_gain*roll+ pitch_gain*pitch)*sin(a_t.at(1));
}

parameter Vestibular::getOutput(unsigned int index)const{
    assert(index<2);
    return ves.at(index);
}


//---------------------ACI Class------------------------//
//Adaptive control input caused by neural couplings of CPGs. It is a type of neural communication
ACI::ACI(unsigned int ID,unsigned int nCPGs){
    gain=1.0;// gain deterime whether the module will be opened or not
    this->ID=ID;
    this->nCPGs =nCPGs;
    output.resize(2);
    cCPGat.resize(nCPGs);
    for(unsigned int i=0;i<nCPGs;i++)
        cCPGat.at(i).resize(2);
    delta.resize(nCPGs);
    for(unsigned int i=0;i<nCPGs;i++)
        delta.at(i).resize(nCPGs);
    K.resize(nCPGs);
    for(unsigned int i=0;i<nCPGs;i++)
        K.at(i).resize(nCPGs);
    C = 0.00;//0.01;
    initK(0.00);
    initDelta();
    CPGSType=CPG_TROT;//default CPGStype in ACI object is a trot
}

void ACI::setInput(parameter gain, matrixD signals){
    this->gain = gain;
    for(unsigned int i=0;i<signals.size();i++)
        for(unsigned int j=0;j<signals.at(i).size();j++)
            cCPGat.at(i).at(j) = signals.at(i).at(j);
}

void ACI::step(){
    for(unsigned int i=0; i<2;i++){
        output.at(i) =0.0;//initial
        for(unsigned int j=0;j<nCPGs;j++){//the number pf CPGs
            output.at(i) -= gain*K.at(ID).at(j)*(C*(1-cos(cCPGat.at(ID).at(i)-cCPGat.at(j).at(i)-delta.at(ID).at(j)))+sin(cCPGat.at(ID).at(i)-cCPGat.at(j).at(i) - delta.at(ID).at(j)));
        }
    }
}

parameter ACI::getOutput(unsigned int index)const{
    assert(index<2);
    return output.at(index);
}

void ACI::initK(parameter constant){
    for(unsigned int i=0;i<nCPGs;i++)
        for(unsigned int j=0;j<nCPGs;j++)
            K.at(i).at(j) = constant;
}

void ACI::initDelta(parameter constant){
    for(unsigned int i=0;i<nCPGs;i++)
        for(unsigned int j=0;j<nCPGs;j++)
            delta.at(i).at(j) = constant;

}

void ACI::setK(){
    K.at(0).at(0) =0.00;
    K.at(0).at(1) =0.01;
    K.at(0).at(2) =0.00;
    K.at(0).at(3) =0.00;

    K.at(1).at(0) =0.00;
    K.at(1).at(1) =0.00;
    K.at(1).at(2) =0.01;
    K.at(1).at(3) =0.00;

    K.at(2).at(0) =0.01;
    K.at(2).at(1) =0.00;
    K.at(2).at(2) =0.00;
    K.at(2).at(3) =0.01;

    K.at(3).at(0) =0.00;
    K.at(3).at(1) =0.00;
    K.at(3).at(2) =0.00;
    K.at(3).at(3) =0.00;

}

void ACI::setDelta(CPGSTYPE CPGSType, Matrix cmatrix){
    if(CPGSType!=this->CPGSType){// does the CPGSType changed
        this->CPGSType=CPGSType;
        switch(CPGSType){
            case CPG_STAND: 
                initK(0.0);// no neural connections
                break;
            case CPG_GALLOP:
                // gallop output
                delta.at(0).at(0) = 0.0;
                delta.at(0).at(1) = -M_PI;
                delta.at(0).at(2) = 0.0;
                delta.at(0).at(3) = -M_PI;

                delta.at(1).at(0) = M_PI;
                delta.at(1).at(1) = 0.0;
                delta.at(1).at(2) = M_PI;
                delta.at(1).at(3) = 0.0;

                delta.at(2).at(0) = 0.0;
                delta.at(2).at(1) = -M_PI;
                delta.at(2).at(2) = 0.0;
                delta.at(2).at(3) = -M_PI;

                delta.at(3).at(0) = M_PI;
                delta.at(3).at(1) = 0.0;
                delta.at(3).at(2) = M_PI;
                delta.at(3).at(3) = 0.0;
                initK(0.01);
                break;
            case CPG_PACE:
                // pace output
                delta.at(0).at(0) = 0.0;
                delta.at(0).at(1) = 0.0;
                delta.at(0).at(2) = -M_PI;
                delta.at(0).at(3) = -M_PI;

                delta.at(1).at(0) = 0.0;
                delta.at(1).at(1) = 0.0;
                delta.at(1).at(2) = -M_PI;
                delta.at(1).at(3) = -M_PI;

                delta.at(2).at(0) = M_PI;
                delta.at(2).at(1) = M_PI;
                delta.at(2).at(2) = 0.0;
                delta.at(2).at(3) = 0.0;

                delta.at(3).at(0) = M_PI;
                delta.at(3).at(1) = M_PI;
                delta.at(3).at(2) = 0.0;
                delta.at(3).at(3) = 0.0;
                initK(0.01);
                break;
            case CPG_WALK_L:
                // a walk_d output
                delta.at(0).at(0) = 0.0;
                delta.at(0).at(1) = -0.5*M_PI;
                delta.at(0).at(2) = -M_PI;
                delta.at(0).at(3) = -1.5*M_PI;

                delta.at(1).at(0) = 0.5*M_PI;
                delta.at(1).at(1) = 0.0;
                delta.at(1).at(2) = -0.5*M_PI;
                delta.at(1).at(3) = -M_PI;

                delta.at(2).at(0) = M_PI;
                delta.at(2).at(1) = 0.5*M_PI;
                delta.at(2).at(2) = 0.0;
                delta.at(2).at(3) = -0.5*M_PI;

                delta.at(3).at(0) = 1.5*M_PI;
                delta.at(3).at(1) = M_PI;
                delta.at(3).at(2) = 0.5*M_PI;
                delta.at(3).at(3) = 0.0;
                initK(0.01);
                break;
            case CPG_WALK_D:
                // a walk_l output
                delta.at(0).at(0) = 0.0;
                delta.at(0).at(1) = -1.5*M_PI;
                delta.at(0).at(2) = -M_PI;
                delta.at(0).at(3) = -0.5*M_PI;

                delta.at(1).at(0) = 1.5*M_PI;
                delta.at(1).at(1) = 0.0;
                delta.at(1).at(2) = 0.5*M_PI;
                delta.at(1).at(3) = M_PI;

                delta.at(2).at(0) = M_PI;
                delta.at(2).at(1) = -0.5*M_PI;
                delta.at(2).at(2) = 0.0;
                delta.at(2).at(3) = 0.5*M_PI;

                delta.at(3).at(0) = 0.5*M_PI;
                delta.at(3).at(1) = -M_PI;
                delta.at(3).at(2) = -0.5*M_PI;
                delta.at(3).at(3) = 0.0;
                initK(0.01);
                break;
            case CPG_TROT: 
                // trot output  4
                delta.at(0).at(0) = 0.0;
                delta.at(0).at(1) = -M_PI;
                delta.at(0).at(2) = -M_PI;
                delta.at(0).at(3) = 0.0;

                delta.at(1).at(0) = M_PI;
                delta.at(1).at(1) = 0.0;
                delta.at(1).at(2) = 0.0;
                delta.at(1).at(3) = M_PI;

                delta.at(2).at(0) = M_PI;
                delta.at(2).at(1) = 0.0;
                delta.at(2).at(2) = 0.0;
                delta.at(2).at(3) = M_PI;

                delta.at(3).at(0) = 0.0;
                delta.at(3).at(1) = -M_PI;
                delta.at(3).at(2) = -M_PI;
                delta.at(3).at(3) = 0.0;
                initK(0.01);
                break;
            case CPG_APC: // 5
                initK(0.0);
                break;
            case CPG_ANC:// Adaptive neural connection 6, the delta was generated through tranfering the adaptive limb movement coordination relationship
                assert(cmatrix.getM()==delta.size());
                for(unsigned int i=0;i<delta.size();i++)
                    for(unsigned int j=0;j<delta.at(i).size();j++)
                        delta.at(i).at(j)=cmatrix.val(i,j);

                initK(0.01);
                break;
            case CPG_APNC:// APM+ANC, adaptive physical and neural communications
                assert(cmatrix.getM()==delta.size());
                for(unsigned int i=0;i<delta.size();i++)
                    for(unsigned int j=0;j<delta.at(i).size();j++)
                        delta.at(i).at(j)=cmatrix.val(i,j);
                initK(0.01);// neural connection gains are zero
                break;
            case CPG_PM:
                initK(0.0);// neural connection gains are zero
                break;
            case CPG_PR:
                initK(0.0);// neural connection gains are zero
                break;
            case CPG_REFLEX: 
                initK(0.0);// no neural connections
                break;
            default:
                perror("CPGSType don't define in cpg.cpp:334");
        }
    }

}





