/***
Author:suntao
Date:2018
Description: It is a ann with hypersis properties
 It can be used to as a switcher, when input > for example 0.4, then the output is around 1.0, otherwise, the output is around zero. 

****/

#include "neuralpreprocessing.h"

#define MASS 2.5 // robot mass
#define GRAVITY 9.8 // earth gravity 
NP::NP(float scale, std::string _transfer,bool learning):count(0),nu(0.01),input(0.0),target(0.0)
{
    this->scale=scale;// the amplitude of the output
    transfer = _transfer;
    learning_state=learning;
    setNeuronNumber(1);
    if(!transfer.compare("logistic")){ // defualt is logistic: sigmoid, y \in [0,1], x\in -6 到 6， 
        setTransferFunction(0,logisticFunction());
        Wi = 10.0;
        Wr=7.2;
        Bias=-6.0;
    }
    else{
        setTransferFunction(0,tanhFunction());
        Wi = 10.0;
        Wr=2.5;
        Bias=0.0;
    }
    if(learning_state==true){
        output=1.0;
        Err=0.0;

        Wi=0.0;
        Wr=0.0;
        Bias=0.0;
    }
    w(0,0,Wr);//7.2 --8.6
    b(0,Bias);

    // parameters for max-min normalization
    input_min=0.0;
    input_max=0.0;

}

/*self-connect neuron for low-pass filter*/
void NP::step(){
    ANN::setInput(0,Wi*input);//input to neuron 0 is Wi*input
    w(0,0,Wr);//self-connection
    b(0,Bias);// bias of neuron 0
    ANN::step();
}

/*low-pass fileter via a typeical way*/
//void NP::step(){
//    ANN::setOutput(0,input);
//}


void NP::stepLearning(float _target){

    target=_target;
    output_old=output;
    NP::step();
    output=ANN::getOutput(0);

    // learning step
    if(learning_state==true){
        count+=1;
        if(count%200==0){
            Err=0.0;
        }
        Err += 1/2.0*(target-output)*(target-output);
        delta =nu*(target-output)*(1-output*output);
        deltaWr = delta*output_old;
        deltaWi = delta*input;
        deltaB = delta;
        Wr+=deltaWr;
        Wi+=deltaWi;
        Bias +=deltaB;
        if((Err < 1.0)&&(count%200==199)){
            learning_state = false;
            std::cout<<"Learning finisied"<<std::endl;
        }
        std::cout<<"ERR: "<<Err<<std::endl;
        std::cout<<"Wr: "<<Wr<<std::endl;
        std::cout<<"Wi: "<<Wi<<std::endl;
        std::cout<<"Bias: "<<Bias<<std::endl;
    }
}
void NP::setUp(float _Wi,float _Wr,float _Bias){
    Bias=_Bias;
    Wi=_Wi;
    Wr=_Wr;
    learning_state=false;// close the learning 
}
float NP::getOutput()
{
    return scale*ANN::getOutput(0);
}

float NP::getWr(){
    return Wr;
}
/* normalize the input  */
void NP::setInput(float _input){
    // get the  border of the input
    if(_input<input_min)
        input_min=_input;
    if(_input>input_max)
        input_max=_input;

    //mormalize the input into 0.0 or 12, 12 is suitable for the NP paprameter setup and lilibot weight/2.0
    if(_input>input_max/3.0){//MASS*GRAVITY/3.0){//if the actual GRF is very high than its normal value (i.e., MASS*GRAVIRT/2.0), normalize the input (i.e., GRF)
        input = 12;//_input/float(input_max-input_min)*MASS*GRAVITY/2.0;
    }else{
        input=0.0;//_input;
    }
}
