
#ifndef NP_H_
#define NP_H_


#include "utils/ann-framework/ann.h"
#include <string.h>
#include <stdlib.h>
#include <iostream>
class NP:public ANN {
public:

	NP(float scale=1.0, std::string _transfer= "logistic",bool learning=false);
	float getOutput();
	float getWr();
	void step();
	void stepLearning(float _target);
	void setInput(float _input);
	void startLearning();
	void setUp(float _Wi,float _Wr,float _Bias);
private:
    float scale;
	unsigned int count;
	float nu,delta, deltaWi,deltaWr,deltaB;
	float Wi,Wr,Bias;
	float input,output,output_old,target,Err;
	bool learning_state;
	string transfer;//the type fo np
    //parameters for max-min normalization
    float input_min;
    float input_max;
};

#endif /* PCPG_H_ */
