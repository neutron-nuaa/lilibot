#ifndef _SPCPG_H
#define _SPCPG_H
//#include <selforg/matrix.h>
#include <vector>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "utils/ann-framework/ann.h"
#include "utils/ann-framework/neuron.h"
#include "learning-tool/stdelayline.h"
#include "learning-tool/matrix.h"
#include "learning-tool/adaptiveFeedbackGain.h"
using namespace matrix;
typedef float parameter;
typedef std::vector<parameter> vectorD;
typedef std::vector<std::vector<parameter>> matrixD;
//typedef std::vector<std::vector<parameter>> Matrix;

enum CPGSTYPE{
//0-5,CCPGs,6-DCPGs
	CPG_STAND = -1,
	CPG_WALK_D = 0,
	CPG_WALK_L = 1,
	CPG_PACE = 2,
	CPG_GALLOP = 3,
	CPG_TROT = 4, // 0-4, predefined neural communications
	CPG_APC =5, // adaptive physical communication
	CPG_ANC =6, // adaptive neural communications
    CPG_APNC=7, // adaptive physical and neural communication
    CPG_PM=8,// PM=tegotae, phase modulation, physical communication with predefined and fixed sensory feedback gain
    CPG_PR=9, // phase resetting
	CPG_REFLEX =10,// only reflex
	CPG_ONLY =11 //only CPG
};
//------------adaptive sensory feedback function----//
class ASF{
public:
	ASF(unsigned int ID);
	void setInput(parameter gain, parameter gamma,parameter grf,vectorD a_t);
	void step();
	parameter getOutput(unsigned int index)const;
private:
    unsigned int ID;
	vectorD o_t;
	vectorD output;
	parameter grf;// grf
    parameter gamma;// adaptive feedback gain
    parameter mg;//robot weight
    parameter gain;// turn on or off this module
};

//-------------PhaseReset class---------//
class PhaseReset{
public:
	PhaseReset(unsigned int ID);
	void setInput(parameter gain, vectorD a_t,parameter grf, parameter threshold);
    parameter prs_threshold; // determine whether to turn the module on or not
	void step();
	parameter getOutput(unsigned int index)const;
private:
	vectorD a_t;
	vectorD a_t_old;
	vectorD reset;
	parameter grf;
	parameter grf_old;
	parameter threshold;
	parameter Diracs;
    parameter mg;// robot weight
    parameter leg_num;// robot leg number
    parameter gain;// turn on or off this module
};
//-----------PhaseInhibition class-----//
class PhaseInhibition{
public:
	PhaseInhibition(unsigned int ID);
	void setInput(parameter gain,parameter grf);
	void step();
	parameter getOutput(unsigned int index)const;
private:
	parameter Ch;
	parameter grf;
	parameter Tduration;
	vectorD inhibition;
    parameter gain;// turn on or off this module
};

//-------------Vestibular class-------------//
class Vestibular{
    public: 
        Vestibular(uint8_t leg);
        void setInput(parameter gain, vector<parameter> ori, parameter grf, vectorD a_t);
        void step();
        parameter getOutput(unsigned int index) const;
    private:
        parameter gain;// this parameter determines the magenitude of the vestibular-based phase modulation effects.
        parameter roll,pitch;
        parameter grf;
        parameter roll_gain,pitch_gain;
        vector<parameter> ves;
        vectorD a_t;
};


//------------ACI class------------//
class ACI{
public:
	ACI(unsigned int ID,unsigned int nCPGs);
	void setInput(parameter gain, matrixD signals);
	void step();
	parameter getOutput(unsigned int index)const;
	void initDelta(parameter constant=0.0);
	void initK(parameter constant);
	void setK();
	void setDelta(CPGSTYPE CPGSType, Matrix cmatrix);
private:
	unsigned int ID;
	unsigned int nCPGs;
	vectorD output;
	matrixD cCPGat;
	matrixD delta;
	matrixD K;
	parameter C;
    CPGSTYPE CPGSType;
    parameter gain;// turn on or off this module
};



//-------------synaptic plasticity CPG class---------------//
class SPCPG :public ANN
{
    private:
        unsigned int ID;//identity of CPG
        unsigned int nCPGs;//the number of CPG
        parameter MI;
		vectorD a_t,a_t1;//activity of neuron
		vectorD o_t;//output of neuron
		//----- adaptive neural communication using adaptive control input------//
		ACI *aci;
		//-----adaptive physical communication using sensory feedback term---//
		ASF * asf;
		// ----phase resetting ------//
		PhaseReset * prs;
		// ---phase inhibition ----//
		PhaseInhibition *pib;	
		// ---vestibular response ----//
		Vestibular *ves;	
        /* Adaptive feedback gain  */
        AdaptiveFeedbackGain *afg; 
public:

		SPCPG(unsigned int ID,unsigned int nCPGs);
		virtual ~SPCPG();
		void updateWeights();
		void updateActivities();
		void updateOutputs();
        void step();
		parameter getOut0();
		parameter getOut1();
		parameter getACIOutput(unsigned int idex)const;
		parameter getASFOutput(unsigned int index)const;
        parameter getAFGOutput()const;
        parameter getFMOutput()const;
	
		void setMi(parameter mi);
		void setEfference(parameter jmc);// joint motor command
		void setCPGSType(CPGSTYPE CPGType);
        void setInput(parameter grf, parameter np_grf, vector<parameter> ori, parameter gain, parameter threshold, matrixD signals, CPGSTYPE CPGType, Matrix cmatrix);
private:
    CPGSTYPE CPGType;// CPG type
    parameter so2_gain;// on/off of so2 neuron activations

    matrixD aci_signals;// for ACI module
    Matrix aci_cmatrix;
    parameter aci_gain; // determine whether to turn the module on or not

    parameter asf_np_grf;// for GRF sensory feedback module (Phase modualtion (PM))
    parameter asf_gamma;// determine the sensory feedback modulation strength
    parameter asf_gain; // determine whether to turn the module on or not

    vector<parameter> ves_ori;// for vestibular sensory feedback module 
    parameter ves_grf;
    parameter ves_gain;// determnine wheter to turn the module on or not

    parameter prs_grf;// for GRF sensory feedback module with phase resetting 
    parameter prs_threshold;
    parameter prs_gain; // determine whether to turn the module on or not

    parameter pib_grf;// for GRF sensory feedback module with phase inhibition
    parameter pib_gain;// determine whether to turn the module on or not


    parameter afg_grf;// GRF for adaptive sensory feedback gain 
    parameter afg_jmc;// Joint motor command for adaptive sensory feedback gain
};
#endif
